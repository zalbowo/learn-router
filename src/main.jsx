import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import {createBrowserRouter, RouterProvider} from 'react-router-dom'
import Root, {loader as rootLoader, action as rootAction} from './routes/root.jsx'
import ErrorPage from './error-pages.jsx'
import Contact, {loader as contactLoader} from './routes/contact.jsx'
import EditContact, {action as editAction} from './routes/edit.jsx'
import { action as destroyAction } from './routes/destroy.jsx'


const router = createBrowserRouter([
  {
    path: "/",
    element: <Root/>,
    errorElement: <ErrorPage/>,
    loader: rootLoader,
    action: rootAction,
    children: [
      {
        path: "contacts/:contactId",
        loader: contactLoader,
        element: <Contact/>
      },
      {
        path: "contacts/:contactId/edit",
        loader: contactLoader,
        action: editAction,
        element: <EditContact/>
      },
      {
        path: "contacts/:contactId/destroy",
        action: destroyAction,
      },
    ]
  },
  
])

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router}/>
  </React.StrictMode>,
)
